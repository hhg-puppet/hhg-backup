# backup

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with backup](#setup)
    * [Setup requirements](#setup-requirements)
    * [Beginning with backup](#beginning-with-backup)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module installs a snapshot cronjob for btrfs.

## Setup

### Setup Requirements

The shapshot tool works for the root directory on btrfs file systems. You
should have second partition or a remote file system mounted, otherwise
each new snapshot will include all the old once.

Backups are generated once a month.

### Beginning with backup

See [Usage](#usage)

## Usage

### Using default values

	class { '::backup':,
		destination => '/var/backup' }

## Reference

### Public Classes

* backup: Main class, installs required packages and sets up the cronjob

### Private Classes

* backup::install: Installs the required packages
* backup::config: Installs the cron job

### Parameters

#### `destination`

The path where the backup is stored.

## Limitations

The module is currently limited to btrfs file systems.

This module has been built on and tested against Puppet 4.

The module has beent tested on Debian Jessie.

## Development

Feel free to open issues or submit merge requests.
