# == Class: backup::config
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class backup::config inherits backup {
	
	file { '/etc/cron.monthly/snapshot':
		owner => 'root',
		group => 'root',
		mode => '755',
		content => template('backup/snapshot.erb')
	}

}
