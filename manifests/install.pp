# == Class: backup::install
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class backup::install {

	package { 'btrfs-tools':
		ensure => 'installed'
	}

	package { 'bzip2':
		ensure => 'installed'
	}
	
}
