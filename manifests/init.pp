# == Class: backup
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class backup
(
	$destination
) {

	validate_string($destination)
	
	include backup::install
	include backup::config

}
